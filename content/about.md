---
title: À propos
description: Détails sur l'application <b>Roazhon STAR Bus</b>.
---

L'application **Roazhon STAR Bus** permet de récupérer les horaires des bus de [**Rennes métropole**](https://metropole.rennes.fr) gérés par le [**STAR**](https://www.star.fr)[^1] dont la maison mère est [**Kéolis**](https://www.keolis.com). Les données récupérées proviennent de l'API du STAR, [**Data Explore**](https://data.explore.star.fr/page/home), en accès public.

Le logo de de l'application a été réalisé d'après celui du STAR et celui de Rennes métropole.

[^1]: service des transports en commun de l'agglomération rennaise.
