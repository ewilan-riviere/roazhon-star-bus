# Roazhon STAR Bus

> Project to get bus hours of STAR of Rennes city in France. Built with NuxtJS and [**data.explore.star.fr**](https://data.explore.star.fr/page/home/)

## Build Setup

Install dependencies and serve with hot reload at localhost:3000

```bash
yarn
yarn dev
```

Build for production and launch server

```bash
yarn build
yarn start
```

Generate static project

```bash
yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

## License

[MIT License, Ewilan Rivière](https://gitlab.com/EwieFairy/roazhon-star-bus/-/blob/master/LICENSE)
