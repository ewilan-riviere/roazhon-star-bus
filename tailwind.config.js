module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
  },
  purge: {
    // Learn more on https://tailwindcss.com/docs/controlling-file-size/#removing-unused-css
    enabled: process.env.NODE_ENV === 'production',
    content: [
      'components/**/*.vue',
      'layouts/**/*.vue',
      'pages/**/*.vue',
      'plugins/**/*.js',
      'nuxt.config.js',
    ],
  },
  theme: {
    extend: {
      fontFamily: {
        'vistasansot-medium': ['VistaSansOT-Medium'],
        mono: [
          'Menlo',
          'Monaco',
          'Consolas',
          'Liberation Mono',
          'Courier New',
          'monospace',
        ],
      },
      screens: {
        xl: '1500px',
        lg: '1100px',
        md: '900px',
        sm: '400px',
      },
      colors: {
        'blue-star-dark': '#002856',
        'blue-star-light': '#53BAAA',
      },
      backgroundImage: {
        'roazhon-bus': "url('/images/roazhon-bus.svg')",
        'parlement-de-bretagne': "url('/images/parlement-de-bretagne.jpg')",
      },
      opacity: {},
    },
  },
  variants: {
    textColor: ['responsive', 'hover', 'focus', 'group-hover'],
    scale: ['responsive', 'hover', 'focus', 'active', 'group-hover'],
    animation: [
      'responsive',
      'motion-safe',
      'motion-reduce',
      'hover',
      'group-hover',
    ],
    translate: ['responsive', 'hover', 'focus', 'active', 'group-hover'],
  },
  plugins: [require('@tailwindcss/typography'), require('@tailwindcss/ui')],
}
