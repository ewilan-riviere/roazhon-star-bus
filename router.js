import Vue from 'vue'
import Router from 'vue-router'

import Home from '~/pages/index'
import About from '~/pages/about'

Vue.use(Router)

export function createRouter() {
  return new Router({
    mode: 'history',
    routes: [
      {
        name: 'home',
        path: '/',
        component: Home,
      },
      {
        name: 'about',
        path: '/a-propos',
        component: About,
      },
    ],
    scrollBehavior(to, from, savedPosition) {
      if (to.hash) {
        return { selector: to.hash }
        // return window.scrollTo({
        //   top: document.querySelector(to.hash).offsetTop,
        //   behavior: 'smooth',
        // })
      } else {
        // return { x: 0, y: 0 }
        setTimeout(() => {
          return window.scrollTo({ top: 0, behavior: 'smooth' })
        }, 100)
      }
    },
  })
}
